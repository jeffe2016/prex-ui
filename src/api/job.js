import { axios } from '@/utils/request'

/**
 * 岗位信息模块
 * @param data
 */

// 新增岗位
export function addJob(data) {
  return axios({
    url: '/sys/job',
    method: 'post',
    data: data
  })
}

// 获取岗位列表
export function queryJobList(parms) {
  return axios({
    url: '/sys/job',
    method: 'get',
    params: parms
  })
}
// 更新岗位
export function updateJob(data) {
  return axios({
    url: '/sys/job',
    method: 'put',
    data: data
  })
}

/**
 * 根据id删除岗位
 * @param id
 */
export function deleteJob(id) {
  return axios({
    url: '/sys/job/' + id,
    method: 'delete'
  })
}

/**
 * 批量删除岗位
 * @param parameter
 * @returns {*}
 */
export function batchDeleteJob(parameter) {
  return axios({
    url: '/sys/job/batchDelete',
    method: 'delete',
    params: parameter
  })
}

// 通过部门id获取岗位列表
export function getJobListByDeptId(id) {
  return axios({
    url: '/sys/job/' + id,
    method: 'get'
  })
}
