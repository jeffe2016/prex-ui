import { axios } from '@/utils/request'

/**
 * 客户端模块
 * @param data
 */

// 新增客户端
export function addClient(data) {
  return axios({
    url: '/sys/client',
    method: 'post',
    data: data
  })
}

// 获取客户端列表
export function queryClientList(parms) {
  return axios({
    url: '/sys/client',
    method: 'get',
    params: parms
  })
}
// 更新客户端
export function updateClient(data) {
  return axios({
    url: '/sys/client',
    method: 'put',
    data: data
  })
}

/**
 * 根据id删除岗位
 * @param id
 */
export function deleteClient(id) {
  return axios({
    url: '/sys/client/' + id,
    method: 'delete'
  })
}

