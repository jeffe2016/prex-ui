import { axios } from '@/utils/request'

/*
 * 部门管理模块
 */

// 保存
export const saveDept = (data) => {
  return axios({
    url: '/sys/dept',
    method: 'post',
    data: data
  })
}
// 获取部门列表
export const getDept = () => {
  return axios({
    url: '/sys/dept',
    method: 'get'
  })
}
// 获取部门树
export const queryDepartTreeList = () => {
  return axios({
    url: '/sys/dept/queryDepartTreeList',
    method: 'get'
  })
}

// 更新部门
export const updateDept = (data) => {
  return axios({
    url: '/sys/dept',
    method: 'put',
    data: data
  })
}
// 删除部门
export const deleteDept = (id) => {
  return axios({
    url: '/sys/dept/' + id,
    method: 'delete'
  })
}

// 批量删除部门
export const batchDeleteDept = (parameter) => {
  return axios({
    url: '/sys/dept/batchDelete',
    method: 'delete',
    params: parameter
  })
}
